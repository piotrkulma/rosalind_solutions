package pl.piotrkulma.rosalind.bioinformaticsstronhold;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public final class FileUtils {
    public static String readContentFromResource(final String fileNamePath) throws IOException {
        InputStream inputStream = FileUtils.class.getResourceAsStream(fileNamePath);

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        String line;
        String content = "";

        while ((line = reader.readLine()) != null) {
            content += line.trim();
        }

        inputStream.close();
        reader.close();

        return content;
    }

    public interface PutSplit<K, V> {
        void putSplitToMap(Map<K, V> map, String[] split);
    }

    public class PutSplitS implements PutSplit<String, String> {
        @Override
        public void putSplitToMap(Map<String, String> map, String[] split) {
            map.put(split[0], split[1]);
        }
    }

    public class PutSplitCD implements PutSplit<Character, Double> {
        @Override
        public void putSplitToMap(Map<Character, Double> map, String[] split) {
            map.put(split[0].charAt(0), Double.valueOf(split[1]));
        }
    }

    public static <K, V> Map<K, V> readPairsFromResource(final String fileNamePath, final PutSplit putSplit) throws IOException {
        InputStream inputStream = FileUtils.class.getResourceAsStream(fileNamePath);

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        String line;
        String split[];

        Map<K, V>  map = new HashMap<>();

        while ((line = reader.readLine()) != null) {
            split = line.split(" ");
            putSplit.putSplitToMap(map, split);
        }

        inputStream.close();
        reader.close();

        return map;
    }
/*
    public static Map<String, String> readPairsFromResource(final String fileNamePath) throws IOException {
        InputStream inputStream = FileUtils.class.getResourceAsStream(fileNamePath);

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        String line;
        String splitted[];
        Map<String, String>  map = new HashMap<>();

        while ((line = reader.readLine()) != null) {
            splitted = line.split(" ");
            map.put(splitted[0], splitted[1]);
        }

        inputStream.close();
        reader.close();

        return map;
    }

    public static Map<Character, Double> readCDPairsFromResource(final String fileNamePath) throws IOException {
        InputStream inputStream = FileUtils.class.getResourceAsStream(fileNamePath);

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        String line;
        String splitted[];
        Map<Character, Double>  map = new HashMap<>();

        while ((line = reader.readLine()) != null) {
            splitted = line.split(" ");
            map.put(splitted[0].charAt(0), Double.valueOf(splitted[1]));
        }

        inputStream.close();
        reader.close();

        return map;
    }*/

    public FileUtils() {
    }
}
