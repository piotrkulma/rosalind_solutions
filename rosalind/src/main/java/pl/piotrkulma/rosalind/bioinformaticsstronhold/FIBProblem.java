package pl.piotrkulma.rosalind.bioinformaticsstronhold;

public class FIBProblem {
    private int n;
    private int k;

    public static void main(final String[] args) {
        FIBProblem problem = new FIBProblem();
        problem.initData(args);
        problem.iterate();
    }

    public void initData(final String[] args) {
        this.n = Integer.parseInt(args[0]);
        this.k = Integer.parseInt(args[1]);
    }

    public void iterate() {
        long youngCount, oldCount;
        long prevYoungCount = 1;
        long prevOldCount = 0;

        int month = 1;

        while (month < this.n) {
            youngCount = prevOldCount * this.k;
            oldCount = prevYoungCount + prevOldCount;
            prevOldCount = oldCount;
            prevYoungCount = youngCount;

            month++;
            System.out.println("Y:" + youngCount + ", O:" + oldCount + ", SUM:" + (youngCount + oldCount));
        }
    }
}
