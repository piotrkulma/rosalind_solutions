package pl.piotrkulma.rosalind.bioinformaticsstronhold;

public class REVCProblem {
    public static void main(final String[] args) {
        REVCProblem problem = new REVCProblem();
        problem.reverseDNAComplement(args[0]);
    }

    public char complementNucleotide(final char n) {
        switch (n) {
            case 'A':
                return 'T';
            case 'T':
                return 'A';
            case 'C':
                return 'G';
            default:
                return 'C';
        }
    }

    public void reverseDNAComplement(final String dnaString) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = dnaString.length() - 1; i >= 0; i--) {
            stringBuilder.append(complementNucleotide(dnaString.charAt(i)));
        }

        System.out.print(stringBuilder.toString());
    }
}
