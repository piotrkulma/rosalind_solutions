package pl.piotrkulma.rosalind.bioinformaticsstronhold;

public class DNAProblem {
    private int aCounter;
    private int cCounter;
    private int gCounter;
    private int tCounter;

    public static void main(final String[] args) {
        DNAProblem dnaProblem = new DNAProblem();
        dnaProblem.countNucleotides(args[0]);
    }

    public DNAProblem() {
        this.aCounter = 0;
        this.cCounter = 0;
        this.gCounter = 0;
        this.tCounter = 0;
    }

    public void countNucleotides(final String dnaString) {
        for(int i=0; i<dnaString.length(); i++) {
            switch (dnaString.charAt(i)) {
                case 'A':
                    this.aCounter ++;
                    break;
                case 'C':
                    this.cCounter ++;
                    break;
                case 'G':
                    this.gCounter ++;
                    break;
                default:
                    this.tCounter ++;
                    break;
            }
        }
        printCountedNucleotides();
    }

    private void printCountedNucleotides() {
        System.out.print(this.aCounter + " " +
                this.cCounter + " " +
                this.gCounter + " " +
                this.tCounter);
    }
}
