package pl.piotrkulma.rosalind.bioinformaticsstronhold;

import java.math.BigInteger;

public class FIBDProblem {
    private int n;
    private int m;

    public static void main(final String[] args) {
        FIBDProblem problem = new FIBDProblem();
        problem.initData();
        problem.iterate();
    }

    public void initData() {
        this.n = Integer.parseInt("99");
        this.m = Integer.parseInt("19");
        //218067459026532066738
    }

    public void iterate() {
        BigInteger prev[] = new BigInteger[this.m];
        BigInteger act[] = new BigInteger[this.m];

        for (int i = 0; i < this.m; i++) {
            prev[i] = BigInteger.ZERO;
            act[i] = BigInteger.ZERO;
        }

        act[0] = BigInteger.ONE;

        int month = 1;

        while (month <= this.n) {
            for (int i = 0; i < this.m; i++) {
                if (prev[i].compareTo(BigInteger.ZERO) == 0) {
                    continue;
                }

                /**remove previous*/
                act[i] = act[i].subtract(prev[i]);

                /**age + 1, move to the next column
                 *if not dead (next column is less than max months
                 */
                if (i + 1 < this.m) {
                    act[i + 1] = act[i + 1].add(prev[i]);
                } else {
                    /**if dead in next generation only reproduce/*/
                    act[0] = act[0].add(prev[i]);
                }

                /**reproduce itself if age is grater
                 * than 0 (one month) and not dead in next generation
                 */
                if (i > 0 && i < this.m - 1) {
                    act[0] = act[0].add(prev[i]);
                }
            }

            System.out.print("month:" + month + " [");
            for (int i = 0; i < this.m; i++) {
                System.out.print(act[i] + " ");
                prev[i] = act[i];
            }
            System.out.print("]\r\n");

            month++;
        }

        BigInteger sum = BigInteger.ZERO;
        for (int i = 0; i < this.m; i++) {
            sum = sum.add(act[i]);
        }
        System.out.println("SUM: " + sum);
    }
}
