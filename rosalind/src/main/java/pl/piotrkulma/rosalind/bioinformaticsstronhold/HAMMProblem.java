package pl.piotrkulma.rosalind.bioinformaticsstronhold;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class HAMMProblem {
    public static void main(final String[] args) throws IOException {
        HAMMProblem problem = new HAMMProblem();
        String[] data = readContentFromResource("/bioinformaticsstronhold/HAMMProblem_dataset02.txt");
        System.out.println(problem.countHamming(data[0], data[1]));
    }

    public static String[] readContentFromResource(final String fileNamePath) throws IOException {

        String[] dataSet = new String[2];
        InputStream inputStream = FileUtils.class.getResourceAsStream(fileNamePath);

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        int counter = 0;
        String line;

        while ((line = reader.readLine()) != null) {
            dataSet[counter++] = line.trim();
        }

        inputStream.close();
        reader.close();

        return dataSet;
    }

    public int countHamming(final String s1, final String s2) {
        int distance = 0;
        for(int i=0; i<s1.length(); i++) {
            if(s1.charAt(i) != s2.charAt(i)) {
                distance++;
            }
        }

        return distance;
    }
}
