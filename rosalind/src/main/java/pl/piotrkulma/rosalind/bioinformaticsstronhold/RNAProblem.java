package pl.piotrkulma.rosalind.bioinformaticsstronhold;

public class RNAProblem {
    public static void main(final String[] args) {
        RNAProblem rnaProblem = new RNAProblem();
        rnaProblem.transcribeToRNA(args[0]);
    }

    public RNAProblem() {
    }

    public void transcribeToRNA(final String dnaString) {
        System.out.print(dnaString.replaceAll("T", "U"));
    }
}
