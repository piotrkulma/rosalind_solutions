package pl.piotrkulma.rosalind.bioinformaticsstronhold;

import java.io.IOException;
import java.util.Map;

public class PRTMProtein {
    private Map<Character, Double> massMap;
    public static void main(final String[] args) throws IOException {
        String dataset = FileUtils.readContentFromResource(
                "/bioinformaticsstronhold/PRTMProtein_dataset02.txt");
        PRTMProtein problem = new PRTMProtein();

        double proteinMass = problem.calculateProteinMass(dataset);
        System.out.println(proteinMass);
    }
    public PRTMProtein() throws IOException {
        this.readMassMap();
    }

    public double calculateProteinMass(final String proteinString) {
        double mass = 0d;

        for(int i=0; i<proteinString.length(); i++) {
            mass += massMap.get(proteinString.charAt(i));
        }

        return mass;
    }

    private void readMassMap() throws IOException {
        this.massMap = FileUtils.readPairsFromResource(
                "/bioinformaticsstronhold/Monoisotopic_mass_table.txt",
                (map, split) -> map.put(split[0].charAt(0), Double.valueOf(split[1])));
    }
}
