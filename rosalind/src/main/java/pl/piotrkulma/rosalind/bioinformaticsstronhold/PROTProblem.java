package pl.piotrkulma.rosalind.bioinformaticsstronhold;

import java.io.IOException;
import java.util.Map;

public class PROTProblem {
    private Map<String, String> rnaCodonTable;
    public static void main(final String[] args) throws IOException {
        String dataset = FileUtils.readContentFromResource(
                "/bioinformaticsstronhold/PROTProblem_dataset02.txt");
        PROTProblem problem = new PROTProblem();
        String proteinString = problem.translateRNAIntoProtein(dataset);
        System.out.println(proteinString);
    }
    public PROTProblem() throws IOException {
        this.readCodonTable();
    }

    public String translateRNAIntoProtein(final String dna) {
        StringBuilder builder = new StringBuilder();
        for(int i=0; i<dna.length(); i+=3) {
            builder.append(rnaCodonTable.get(dna.substring(i, i+3)));
        }

        return builder.toString();
    }

    private void readCodonTable() throws IOException {
        this.rnaCodonTable = FileUtils.readPairsFromResource(
                "/bioinformaticsstronhold/RNA_codon_table.txt",
                (map, split) -> map.put(split[0], split[1]));
    }
}
