package pl.piotrkulma.rosalind.bioinformaticsstronhold;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class GCProblem {
    private static final String SEPARATOR = ">Rosalind_";
    private static final String NAME_BEGIN = "Rosalind_";

    private double longestGC = 0d;
    private String longestSequenceContent;
    private String longestSequenceName;

    public static void main(final String[] args) throws IOException {
        GCProblem problem = new GCProblem();
        problem.readAndParse("/bioinformaticsstronhold/GCProblem_dataset02.txt");
    }

    public void readAndParse(final String fileNamePath) throws IOException {

        String content = FileUtils.readContentFromResource(fileNamePath);
        String[] segments = content.split(SEPARATOR);

        for (int i = 1; i < segments.length; i++) {
            checkSequence(segments[i].substring(0, 4), segments[i].substring(4));
        }

        System.out.println(this.longestSequenceContent);
        System.out.println(NAME_BEGIN + this.longestSequenceName + " " + this.longestGC + "%");
    }

    private void checkSequence(final String currentSequenceName, String sequenceContent) {
        double currentGC = countGCContent(sequenceContent);

        if (currentGC > this.longestGC) {
            this.longestGC = currentGC;
            this.longestSequenceName = currentSequenceName;
            this.longestSequenceContent = sequenceContent;
        }
    }

    public double countGCContent(final String dnaSequence) {
        double gcCount = 0;
        for (int i = 0; i < dnaSequence.length(); i++) {
            if (dnaSequence.charAt(i) == 'C' || dnaSequence.charAt(i) == 'G') {
                gcCount++;
            }
        }

        return (gcCount / (double) dnaSequence.length()) * 100d;
    }
}
