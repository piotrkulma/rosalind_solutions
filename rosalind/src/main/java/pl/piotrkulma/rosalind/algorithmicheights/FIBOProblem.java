package pl.piotrkulma.rosalind.algorithmicheights;

public class FIBOProblem {
    public static void main(String[] args) {
        FIBOProblem problem = new FIBOProblem();
        problem.fibNum(0);
        problem.fibNum(1);
        problem.fibNum(2);
        problem.fibNum(3);
        problem.fibNum(4);
        problem.fibNum(5);
        problem.fibNum(6);
        problem.fibNum(7);
        problem.fibNum(24);
    }

    public void fibNum(int n) {
        long prev[] = {0, 1};
        long sum = n < 2 ? prev[n] : 0;

        for (int i = 0; i < n - 1; i++) {
            sum = prev[0] + prev[1];
            prev[0] = prev[1];
            prev[1] = sum;
        }

        System.out.println("RESULT fib(" + n + "): " + sum);

    }
}
